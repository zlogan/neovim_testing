#!/bin/zsh

# Install python2 python3 curl pip2 pip3 
apk add python2 python3 python2-dev python3-dev curl

# Alpine-SDK
apk add alpine-sdk

apk update 
apk upgrade 
apk add gcc gnupg curl ruby bash procps musl-dev make linux-headers zlib zlib-dev openssl openssl-dev libssl1.1 

# ZSH, Git, wget, unzip
apk add git wget unzip 

# NeoVim and Python2, Python3 Integration
apk add neovim

# Oh-My-ZSH
apk add zsh

# FuzzySearch for NVim und ZSH
apk add fzf
