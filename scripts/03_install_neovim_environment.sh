#!/bin/zsh

export VIMCONFIG=$HOME/.config/nvim

# NeoVim Standard-Configuration
# mkdir -p $VIMCONFIG 
cp ../ModernVim_Examples/code/init.vim $VIMCONFIG/init.vim

# Vi/Vim-Compability
# mkdir -p $HOME/.vim/after && echo '' > $HOME/.vim/vimrc

echo -e "packadd minpac\ncall minpac#init()" >> $VIMCONFIG/init.vim
