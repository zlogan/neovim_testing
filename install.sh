#!/bin/zsh
export my_dir=${0:a:h}
export config_dir="$my_dir/config"
echo $config_dir

ln -s $config_dir/nvim $HOME/.config/nvim 
ln -s $config_dir/zshrc $HOME/.zshrc
ln -s $config_dir/gitconfig $HOME/.gitconfig
