set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vim/vimrc
" MinPac Package/Plugin-Manager
packadd minpac
call minpac#init()

" minpac must have {'type': 'opt'} so that it can be loaded with `packadd`.
call minpac#add('k-takata/minpac', {'type': 'opt'})

" Add other plugins here.
" FuzzyFileFinder
call minpac#add('junegunn/fzf')
call minpac#add('junegunn/fzf.vim')
nnoremap <C-p> :<C-u>FZF<CR>
" Project-Manager
call minpac#add('tpope/vim-projectionist', {'type': 'opt'})
" Git Integration and Diff-View
call minpac#add('tpope/vim-fugitive')
" Load the plugins
packloadall

" Mouse Integration in Normal and Visual Mode
set mouse=nv
nnoremap <F12> <C-]>
